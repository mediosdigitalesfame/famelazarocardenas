<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Autos</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    	<?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Vehículos</h2>
				</div>
			</div>

					 <!-- INICIO VEHICULOS   -->
	 <div class="portfolio-box with-3-col">
		 <div class="container">
			 <ul class="filter">
				 <li><a href="#" class="active" data-filter="*"><i class="fa fa-th"></i>Mostrar todos</a></li>
				 <li><a href="#" data-filter=".auto">Autos</a></li>
				 <li><a href="#" data-filter=".suv">SUV's - Minivan</a></li>
				 <li><a href="#" data-filter=".pickup">Pick Up's</a></li>
				 <li><a href="#" data-filter=".comerciales">Comerciales</a></li>
             </ul>

			 <div class="portfolio-container">

	             
	             <div class="work-post auto">
					 <div class="work-post-gal">
						 <img alt="Beat HB 2019" src="images/original/beathb2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> BEAT HB<sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>
						 <span><a href="pdfs/beathb2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				  <div class="work-post auto">
					 <div class="work-post-gal">
						 <img alt="Cavalier 2019" src="images/original/cavalier2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> CAVALIER <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>
						 <span><a href="pdfs/cavalier2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				  <div class="work-post auto">
					 <div class="work-post-gal">
						 <img alt="Spark 2019" src="images/original/spark2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> SPARK <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>
						 <span><a href="pdfs/spark2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>



				 <div class="work-post auto">
					 <div class="work-post-gal">
						 <img alt="Beat NB 2019" src="images/original/beatnb2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> BEAT NB <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>
						 <span><a href="pdfs/beatnb2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				

				 <div class="work-post suv">
					 <div class="work-post-gal">
						 <img alt="Equinox 2019" src="images/original/equinox2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> EQUINOX <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>
						 <span><a href="pdfs/equinox2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>                    

				 <div class="work-post auto">
					 <div class="work-post-gal">
						 <img alt="Aveo 2019" src="images/original/aveo2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> AVEO <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>
						 <span><a href="pdfs/aveo2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				 <div class="work-post pickup">
					 <div class="work-post-gal">
						 <img alt="Chetenne 2018" src="images/original/cheyenne2018.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> CHEYENNE <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/cheyenne2018.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>       

				 <div class="work-post pickup">
					 <div class="work-post-gal">
						 <img alt="Colorado 2019" src="images/original/colorado2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> COLORADO <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/colorado2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>    

				 <div class="work-post auto">
					 <div class="work-post-gal">
						 <img alt="Cruze 2018" src="images/original/cruze2018.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> CRUZE <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>
						 <span><a href="pdfs/cruze2018.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				 <div class="work-post suv">
					 <div class="work-post-gal">
						 <img alt="Suburban 2019" src="images/original/suburban2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> SUBURBAN <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/suburban2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>
	                        
				 <div class="work-post suv">
					 <div class="work-post-gal">
						 <img alt="Tahoe 2019" src="images/original/tahoe2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> TAHOE <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font> 
						 <span><a href="pdfs/tahoe2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				 <div class="work-post pickup">
					 <div class="work-post-gal">
						 <img alt="Tornado 2019" src="images/original/tornado2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> TORNADO <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/tornado2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>
	                        
				 <div class="work-post suv">
					 <div class="work-post-gal">
						 <img alt="Trax 2019" src="images/original/trax2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> TRAX <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/trax2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				 <div class="work-post auto">
					 <div class="work-post-gal">
						 <img alt="Malibu 2018" src="images/original/malibu2018.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> MALIBU <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/malibu2018.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				 <div class="work-post suv">
					 <div class="work-post-gal">
						 <img alt="Traverse 2019" src="images/original/traverse2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> TRAVERSE <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/traverse2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				  <div class="work-post pickup">
					 <div class="work-post-gal">
						 <img alt="Silverado 1500 2018" src="images/original/silverado2500.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> SILVERADO 1500 <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/silverado15002018.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				 <div class="work-post pickup">
					 <div class="work-post-gal">
						 <img alt="Silverado 2500 2018" src="images/original/silverado2500.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> SILVERADO 2500 <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/silverado25002018.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				 <div class="work-post comerciales">
					 <div class="work-post-gal">
						 <img alt="Silverado 3500 2018" src="images/original/silverado3500.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> SILVERADO 3500 <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/silverado35002018.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

				 <div class="work-post comerciales">
					 <div class="work-post-gal">
						 <img alt="Express 2019" src="images/original/express2019.jpg">
					 </div>
					 <div class="work-post-content">
						 <h2> <strong> <em> EXPRESS <sup>&reg;</sup> <font color="#f5bc05">2019</font> </em> </strong></h2> </font>  
						 <span><a href="pdfs/express2019.pdf" target="_blank">Ficha técnica</a></span>
					 </div>
				 </div>

  		     </div>	

	     </div>
     </div>

		</div>

 <?php include('contenido/footer.php'); ?>
</body>
</html>