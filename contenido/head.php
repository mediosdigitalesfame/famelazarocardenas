	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="Title" content="Chevrolet Lázaro Cárdenas, Michoacán">
	<meta name="description" content="Te brindamos información sobre Autos, Pick Up's, SUV's, Vans, Vehículos Comerciales y todos los servicios que Chevrolet® ofrece para tí. Piensa en auto, piensa en FAME. Sitio WEB oficial FAME Chevrolet Lázaro Cárdenas. ">
    <meta name="keywords" content="Chevrolet lazaro cardenas, Lazaro Cardenas, Lazaro Cardenas, chevrolet cupatitzio, chevrolet cupatitzio lazaro cardenas, chevrolet fame, fame cupatitzio, Grupo Fame lazaro cardenas, cupatitzio lazaro cardenas, fame cupatitzio lazaro cardenas, chevrolet michoacan, autos usados lazaro cardenas, autos en venta en lazaro cardenas, fame lazaro cardenas, fame Michoacan, México, Autos lazaro cardenas, Nuevos lazaro cardenas, Seminuevos lazaro cardenas, Agencia lazaro cardenas, Servicio lazaro cardenas, Taller de servicio lazaro cardenas, Hojalatería, hojalateria en lazaro cardenas, chevrolet Fame Lázaro Cárdenas, Pintura, postventa, chevrolet Matiz, chevrolet Spark, chevrolet aveo, chevrolet sonic, chevrolet cruze, chevrolet malibu, chevrolet camaro, chevrolet traverse, chevrolet tahoe, chevrolet suburban, chevrolet colorado, chevrolet silverado, chevrolet cheyenne, chevrolet 2016, chevrolet Lujo, chevrolet 2016, chevrolet 2018, financiamiento, carros, agencia, planes, creditos">
    <meta name="author" content="Grupo FAME división Automotriz">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fullwidth.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
   

    <link rel="icon" type="image/png" href="/images/favicon.png" />
    
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
</script>
